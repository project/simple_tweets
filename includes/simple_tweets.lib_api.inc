<?php
/**
 * @file
 * This file include libraries API functions from Simple tweets module.
 */

/**
 * Implements hook_libraries_info().
 */
function simple_tweets_libraries_info() {
  $libraries['Twitter-Post-Fetcher'] = array(
    'name' => 'Twitter-Post-Fetcher',
    'vendor url' => 'https://github.com/jasonmayes/Twitter-Post-Fetcher',
    'download url' => 'https://github.com/jasonmayes/Twitter-Post-Fetcher/archive/master.zip',
    'version arguments' => array(
      'file' => 'js/twitterFetcher.js',
      'pattern' => '/v(\d+\.+\d+)/',
      'lines' => 3,
    ),
    'files' => array(
      'js' => array(
        'js/twitterFetcher_min.js',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_requirements().
 */
function simple_tweets_requirements($phase) {
  $requirements = array();
  $library = libraries_detect('Twitter-Post-Fetcher');
  //print_r($library['version']);die();
  if (empty($library['installed'])) {
    if ($phase == 'runtime') :
      $requirements['show_library'] = array(
        'title' => t('Simple tweets module'),
        'value' => t('Twitter-Post-Fetcher library not installed'),
        'severity' => REQUIREMENT_ERROR,
        'weight' => '-1',
      );
    endif;
  }
  elseif (version_compare($library['version'], '15.0', '>=')) {
    if ($phase == 'runtime') :
      $requirements['show_library'] = array(
        'title' => t('Simple tweets module'),
        'value' => t('Detected good version of "Twitter-Post-Fetcher" library'),
        'severity' => REQUIREMENT_OK,
        'weight' => '-1',
      );
    endif;
  }
  elseif (version_compare($library['version'], '15.0', '<')) {
    if ($phase == 'runtime') :
      $requirements['show_library'] = array(
        'title' => t('Simple tweets module'),
        'value' => t('Detected old version of "Twitter-Post-Fetcher" library, please upgrade it to version 15'),
        'severity' => REQUIREMENT_WARNING,
        'weight' => '-1',
      );
    endif;
  }
  return $requirements;
}
