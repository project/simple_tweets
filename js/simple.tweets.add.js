/**
 * @file
 * This file include reciving data from drupal and call twitterFetcher function.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.simple_tweets = {
    attach: function (context, settings) {

      function customCallbackFunction(tweets) {
        var len = tweets.length;
        var html = '<ul>';
        for (var i = 0; i < len; i++) {
          html += '<li>' + tweets[i] + '</li>';
        }
        html += '</ul>';
        if (len === 0) {
          html += '<p class="error">' + Drupal.t('Failed to get tweets. Check widget ID') + '</p>';
        }
        document.getElementById('block-simple-tweets-t-w-block').innerHTML = html;
      }

      var config = {
        id: settings.simple_tweets_id,
        domId: 'block-simple-tweets-t-w-block',
        maxTweets: settings.simple_tweets_max,
        enableLinks: settings.simple_tweets_hyperlink,
        showUser: settings.simple_tweets_user,
        showTime: settings.simple_tweets_time,
        showRetweet: settings.simple_tweets_retweet,
        showInteraction: settings.simple_tweets_interact,
        showImages: settings.simple_tweets_img,
        lang: settings.simple_tweets_lang,
        linksInNewWindow: settings.simple_tweets_wind,
        customCallback: customCallbackFunction
      };

      twitterFetcher.fetch(config);
      document.getElementById('block-simple-tweets-t-w-block').classList.add('processed-simple-tweets');
    }
  };
})();
